/***************************************************************************
 *                                                                         *
 *                                Help.java                                *
 *                            -------------------                          *
 *   date                 : 10.09.2004, 12:00                              *
 *   copyright            : (C) 2004-2008 Distributed and                  *
 *                              Mobile Systems Group                       *
 *                              Lehrstuhl fuer Praktische Informatik       *
 *                              Universitaet Bamberg                       *
 *                              http://www.uni-bamberg.de/pi/              *
 *   email                : sven.kaffille@uni-bamberg.de                   *
 *                          karsten.loesing@uni-bamberg.de                 *
 *                                                                         *
 *                                                                         *
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   A copy of the license can be found in the license.txt file supplied   *
 *   with this software or at: http://www.gnu.org/copyleft/gpl.html        *
 *                                                                         *
 ***************************************************************************/
package de.uniba.wiai.lspi.chord.console.command;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.text.NumberFormat;
import java.io.*;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.util.Date;
import java.text.DateFormat;


import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

import de.uniba.wiai.lspi.util.console.Command;
import de.uniba.wiai.lspi.util.console.ConsoleThread;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

//for insert
import de.uniba.wiai.lspi.chord.console.command.entry.Key;
import de.uniba.wiai.lspi.chord.console.command.entry.Value;
import de.uniba.wiai.lspi.chord.service.Chord;
import de.uniba.wiai.lspi.util.console.Command;
import de.uniba.wiai.lspi.util.console.ConsoleException;



import java.io.PrintStream;

//import de.uniba.wiai.lspi.util.logging.Logger;

/**
 * This command prints a list of available commands. 
 * Just type <code>help</code> 
 * into the {@link de.uniba.wiai.lspi.chord.console.Main console}.
 * 
 * @author sven
 * @version 1.0.5
 */
public class ReducePageRank extends Command {

 //  private static Logger logger = Logger.getLogger(Help.class.getName());

 /**
  * The name of this {@link Command}. 
  */

 private JFrame mainFrame;
 public static final String COMMAND_NAME = "reducePage";

 /** 
  * Creates a new instance of Help 
  * @param toCommand1 
  * @param out1 
  */
 public ReducePageRank(Object[] toCommand1, PrintStream out1) {
  super(toCommand1, out1);
 }

 public void exec() {
  Object factory = ConsoleThread.getConsole().getCommandFactory();
  // out.println("Factory class " + factory.getClass());
  Field[] fields = factory.getClass().getDeclaredFields();
  // out.println("Number of factory fields " + fields.length);
  Field mapping = null;
  try {
   

   long totaltime = 0, runn = 0, ic = 0, times = 0, totalMatch = 0;
   long parentLength = 0;
   //  for(ic=0;ic<10;ic++){reader = new BufferedReader(new FileReader("ip.txt"));runn=0;ic=0;
   this.out.println("outside while");
   int i=0,j=0;
   int max=0;
   int min=999999;
   //starting computation
   long startSystemTimeNano = System.currentTimeMillis();
    File folder = new File("inputs/");
    File[] listOfFiles = folder.listFiles();
    for (i = 0; i < listOfFiles.length; i++) {
    File file = listOfFiles[i];
      if (file.isFile() && file.getName().endsWith(".txt")) {
        BufferedReader reader = new BufferedReader(new FileReader(String.valueOf(file)));
        String line;
        while ((line = reader.readLine()) != null ) {
          String[] str_array = line.split(" ",4);
          String lang = str_array[0]; 
          String page_details = str_array[1]+"\n";
          String count=str_array[3];
          int co=Integer.parseInt(count);
          if(co>max)max=co;
          if(co<min)min=co;
          
          this.out.println(str_array[0]+"::"+str_array[1]+"::"+str_array[2]+"::val"+str_array[3]);
          j++;
          /*if(j>1000){
              break;
          }*/
        }
      } 
     
  }
  final int m1=max;
  final int m2=min;
  this.out.println( "Reduce TASK Completed\nMax Page Hits:" + String.valueOf(m1)+"\n"+"Min Page Hits:" + String.valueOf(m2));
  //end of comupation
  long stopSystemTimeNano = System.currentTimeMillis();
  this.out.println("TIME FOR THIS SET:--");
  this.out.println(stopSystemTimeNano - startSystemTimeNano);
      
  Runnable r1 = new Runnable() {
    public void run() {
     JOptionPane.showMessageDialog(mainFrame, "Reduce TASK Completed\nMax Page Hits:" + String.valueOf(m1)+"\n"+"Min Page Hits:" + String.valueOf(m2));

    }
   };

/*
   String key = "result";
   long matchPerc = (totalMatch * 100) / parentLength;
   matchPerc = matchPerc;
   this.out.println(matchPerc);
   final String value = String.valueOf(matchPerc);
   this.out.println("totalMatch:" + value);
   Chord chord = ((RemoteChordNetworkAccess) this.toCommand[1]).getChordInstance();

   Key keyObject = new Key(key);
   Value valueObject = new Value(value);
   this.out.println("TOTAL TIME FOR THIS MAP TASK:");
   this.out.println(totaltime);
   Runnable r1 = new Runnable() {
    public void run() {
     JOptionPane.showMessageDialog(mainFrame, "MAP TASK Completed\nResults sent to Reduce Node\nMatching Percentage:" + value);

    }
   };
   new Thread(r1).start();
   try {
    chord.insert(keyObject, valueObject);
    this.out.println("Trying to insert.");

   } catch (Throwable t) {
    ConsoleException e
     = new ConsoleException("Exception during execution of command. " + t.getMessage(), t);
    throw e;
   }*/



   //createServer();
   //      }
  } catch (Exception e) {

  }


 }

 public void writeResultUtil(String LCS) throws Throwable {
  try {
   writeResult(LCS);
  } catch (Exception E) {}
 }
 public String getCommandName() {
  return COMMAND_NAME;
 }

 public void printOutHelp() {
  this.out
   .println("Display a list of all commands available in this console.");
 }
 public String computeLCS(String text, String pattern) {
  String x = text;
  String y = pattern;
  int M = x.length();
  int N = y.length();

  // opt[i][j] = length of LCS of x[i..M] and y[j..N]
  int[][] opt = new int[M + 1][N + 1];

  // compute length of LCS and all subproblems via dynamic programming
  for (int i = M - 1; i >= 0; i--) {
   for (int j = N - 1; j >= 0; j--) {
    if (x.charAt(i) == y.charAt(j))
     opt[i][j] = opt[i + 1][j + 1] + 1;
    else
     opt[i][j] = Math.max(opt[i + 1][j], opt[i][j + 1]);
   }
  }

  // recover LCS itself and print it to standard output
  int i = 0, j = 0;
  String ret = "";
  this.out.print("DNA Matching:");
  while (i < M && j < N) {
   if (x.charAt(i) == y.charAt(j)) {
    this.out.print(x.charAt(i));
    ret += x.charAt(i);
    i++;
    j++;
   } else if (opt[i + 1][j] >= opt[i][j + 1]) i++;
   else j++;
  }
  this.out.print("\n");
  return ret;
 }
 public void createServer() throws IOException {
  ServerSocket servsock = new ServerSocket(10001);
  File myFile = new File("activity1.txt");
  while (true) {
   Socket sock = servsock.accept();
   byte[] mybytearray = new byte[(int) myFile.length()];
   BufferedInputStream bis = new BufferedInputStream(new FileInputStream(myFile));
   bis.read(mybytearray, 0, mybytearray.length);
   OutputStream os = sock.getOutputStream();
   os.write(mybytearray, 0, mybytearray.length);
   os.flush();
   sock.close();
  }

 }
 public void writeResult(String str) throws IOException {
  BufferedWriter writer = null;
  File logFile = new File("result.txt");
  writer = new BufferedWriter(new FileWriter(logFile));

  try {
   writer.write(str);
   writer.newLine();
  } catch (Exception e) {
   e.printStackTrace();
  } finally {
   try {
    // Close the writer regardless of what happens...
    writer.close();
   } catch (Exception e) {}
  }
 }

}