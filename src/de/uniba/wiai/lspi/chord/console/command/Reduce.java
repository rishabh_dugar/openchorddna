/***************************************************************************
 *                                                                         *
 *                                Help.java                                *
 *                            -------------------                          *
 *   date                 : 10.09.2004, 12:00                              *
 *   copyright            : (C) 2004-2008 Distributed and                  *
 *                              Mobile Systems Group                       *
 *                              Lehrstuhl fuer Praktische Informatik       *
 *                              Universitaet Bamberg                       *
 *                              http://www.uni-bamberg.de/pi/              *
 *   email                : sven.kaffille@uni-bamberg.de                   *
 *                          karsten.loesing@uni-bamberg.de                 *
 *                                                                         *
 *                                                                         *
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   A copy of the license can be found in the license.txt file supplied   *
 *   with this software or at: http://www.gnu.org/copyleft/gpl.html        *
 *                                                                         *
 ***************************************************************************/
package de.uniba.wiai.lspi.chord.console.command;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.text.NumberFormat;
import java.io.*;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.util.Date;
import java.text.DateFormat;


import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

import de.uniba.wiai.lspi.util.console.Command;
import de.uniba.wiai.lspi.util.console.ConsoleThread;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

//for insert
import de.uniba.wiai.lspi.chord.console.command.entry.Key;
import de.uniba.wiai.lspi.chord.console.command.entry.Value;
import de.uniba.wiai.lspi.chord.service.Chord;
import de.uniba.wiai.lspi.util.console.Command;
import de.uniba.wiai.lspi.util.console.ConsoleException;

//for reduce
import de.uniba.wiai.lspi.chord.console.command.entry.Key;
import de.uniba.wiai.lspi.chord.service.Chord;
import de.uniba.wiai.lspi.util.console.Command;
import de.uniba.wiai.lspi.util.console.ConsoleException;

import java.io.Serializable;
import java.util.Set;

import java.io.PrintStream;
import java.lang.*;

//import de.uniba.wiai.lspi.util.logging.Logger;

/**
 * This command prints a list of available commands. 
 * Just type <code>help</code> 
 * into the {@link de.uniba.wiai.lspi.chord.console.Main console}.
 * 
 * @author sven
 * @version 1.0.5
 */
public class Reduce extends Command {

    //  private static Logger logger = Logger.getLogger(Help.class.getName());

    /**
     * The name of this {@link Command}. 
     */

    private JFrame mainFrame;
    public static final String COMMAND_NAME = "Reduce";

    /** 
     * Creates a new instance of Help 
     * @param toCommand1 
     * @param out1 
     */
    public Reduce(Object[] toCommand1, PrintStream out1) {
        super(toCommand1, out1);
    }

    public void exec() {
        Object factory = ConsoleThread.getConsole().getCommandFactory();
        // out.println("Factory class " + factory.getClass());
        Field[] fields = factory.getClass().getDeclaredFields();
        // out.println("Number of factory fields " + fields.length);
        Field mapping = null;
        try {
            

             mainFrame = new JFrame("Java Swing Examples");
            mainFrame.setSize(400,400);

       //REDUCE
                String key="result";
                Key keyObject = new Key(key);
        
        Chord chord = ((RemoteChordNetworkAccess)this.toCommand[1]).getChordInstance(); 
        try {
            Set<Serializable> vs = chord.retrieve(keyObject);
            Object[] values = vs.toArray(new Object[vs.size()]); 
            this.out.println("Results " + "in percentages of all nodes: ");
            int total=0;
            int i;
            for (i = 0; i < values.length; i++) {
                this.out.print(values[i]);
                //this.out.print("here");
                int temp=Integer.valueOf(values[i].toString());
                total=total+temp;
                //this.out.println(total);
                if ( !(i == (values.length-1)) ) {
                    this.out.print(",");
                }
                this.out.print(" ");
            }
            this.out.println(total);
            if(total==0)
                        JOptionPane.showMessageDialog(mainFrame, "No Nodes have completed MAP Task \n");
               
            int perc=total/i;
            final int ii=i;
            final String per=Integer.toString(perc);
            this.out.println();
                        Runnable r = new Runnable() {
                public void run() {                    
                    if(ii!=0)
                        JOptionPane.showMessageDialog(mainFrame, "Executing Reduce TASK..\nTotal Percentage Match Between Parent and Child is \n"+per);
                          
                }
                };
                new Thread(r).start();

        }
        catch (Throwable t){
            ConsoleException e = new ConsoleException("Exception during execution of command. " + t.getMessage());
            e.setStackTrace(t.getStackTrace());
            throw e;
        }
            //createServer();
            //      }
        } catch (Exception e) {

        }



        //write result to a file



        //catch(Exception e){

        //}
        //

    }

    public void writeResultUtil(String LCS) throws Throwable {
        try {
            writeResult(LCS);
        } catch (Exception E) {}
    }
    public String getCommandName() {
        return COMMAND_NAME;
    }

    public void printOutHelp() {
        this.out
            .println("Display a list of all commands available in this console.");
    }
    public String computeLCS(String text, String pattern) {
        String x = text;
        String y = pattern;
        int M = x.length();
        int N = y.length();

        // opt[i][j] = length of LCS of x[i..M] and y[j..N]
        int[][] opt = new int[M + 1][N + 1];

        // compute length of LCS and all subproblems via dynamic programming
        for (int i = M - 1; i >= 0; i--) {
            for (int j = N - 1; j >= 0; j--) {
                if (x.charAt(i) == y.charAt(j))
                    opt[i][j] = opt[i + 1][j + 1] + 1;
                else
                    opt[i][j] = Math.max(opt[i + 1][j], opt[i][j + 1]);
            }
        }

        // recover LCS itself and print it to standard output
        int i = 0, j = 0;
        String ret = "";
        this.out.print("DNA Matching:");
        while (i < M && j < N) {
            if (x.charAt(i) == y.charAt(j)) {
                this.out.print(x.charAt(i));
                ret += x.charAt(i);
                i++;
                j++;
            } else if (opt[i + 1][j] >= opt[i][j + 1]) i++;
            else j++;
        }
        this.out.print("\n");
        return ret;
    }
    public void createServer() throws IOException {
        ServerSocket servsock = new ServerSocket(10001);
        File myFile = new File("activity1.txt");
        while (true) {
            Socket sock = servsock.accept();
            byte[] mybytearray = new byte[(int) myFile.length()];
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(myFile));
            bis.read(mybytearray, 0, mybytearray.length);
            OutputStream os = sock.getOutputStream();
            os.write(mybytearray, 0, mybytearray.length);
            os.flush();
            sock.close();
        }

    }
    public void writeResult(String str) throws IOException {
        BufferedWriter writer = null;
        File logFile = new File("result.txt");
        writer = new BufferedWriter(new FileWriter(logFile));

        try {
            writer.write(str);
            writer.newLine();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {}
        }
    }

}