/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clientframework;

import ClientRemoteInterface.ServerInitiator;
import java.io.BufferedReader;
import java.io.DataOutputStream;

/**
 *
 * @author Prashant Mishra
 */
public class remotehandler implements Runnable{

    int VMPORT;
    DataOutputStream outToServer;
    public remotehandler(int port,DataOutputStream outToServer) {
    VMPORT=port;
    this.outToServer=outToServer;
    }

    
    @Override
    public void run() {
        System.out.println("VM "+VMPORT); 
        ServerInitiator remote=new ServerInitiator();
        remote.initialize(VMPORT,outToServer);
    }
    
}
